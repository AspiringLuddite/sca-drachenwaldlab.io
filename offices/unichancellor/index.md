---
title: University Chancellor
featureimg: images/foo/test.jpg
featurealt: thing
---
<h3>Chancellor of the University</h3>
<p>Ragnhil de Kaxtone (Asa Hestner-Blomqvist)</p>
<p><script type="text/javascript">document.write(String.fromCharCode(60,97,32,104,114,101,102,61,39,109,97,105,108,116,111,58,117,110,105,118,101,114,115,105,116,121,64,100,114,97,99,104,101,110,119,97,108,100,46,115,99,97,46,111,114,103,39,62,117,110,105,118,101,114,115,105,116,121,64,100,114,97,99,104,101,110,119,97,108,100,46,115,99,97,46,111,114,103,60,47,97,62));</script></p>
<h3>Minimum University Site Requirements</h3>
<p>University is an easy kingdom event to run, since classes (both arranging for instructors and the schedule of classes) are arranged by the University Chancellor. However, the site should have the following:</p>
<ul>
<li>Space for at least 8 simultaneous tracks of classes </li>
<li>A hall for feast that will hold at least 120 people </li>
<li>Sleeping space for at least 120 people </li>
<li>Indoor or outdoor space to hold fighting related classes </li>
</ul>
<p>Exceeding these requirements would, of course, be better, but that is the minimum.</p>
<h3>Chancellor Policies :</h3>
<p>See section 3 of <a href="{{ site.baseurl }}{% link offices/unichancellor/policies.md %}">Policies of Arts and Sciences for the Kingdom of Drachenwald</a></p>


{% include officer-contacts.html %}