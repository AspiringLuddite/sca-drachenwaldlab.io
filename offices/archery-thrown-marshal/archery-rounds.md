---
title: Archery rounds and scoresheets 
excerpt: Archery competition rules, with scoresheets to download
toc: true
toc_label: Contents
---

* [Archery round quick reference]({{ site.baseurl }}{% link offices/archery-thrown-marshal/files/archery-round-quick-reference.pdf %}) reminds you which round has which distances and how many arrows to shoot.
* [Royal round scoresheet as Excel file]({{ site.baseurl }}{% link offices/archery-thrown-marshal/files/royal-round-score-sheet.xls %})
* [Inter-Kingdom Archery Competition, see IKAC website](http://scores-sca.org/home/index.php?R=10)
* [Portsmouth round scoresheet as Excel file]({{ site.baseurl }}{% link offices/archery-thrown-marshal/files/portsmouth-round-score-sheet.xls %})

# Royal Round

The most common competition in the Society is the Royal Round.

* The Royal Round uses a standard 5-ringed 60 cm FITA target.
* Archers shoot one round of 6 arrows at 40, 30, and 20 yards, as well as, a timed round with unlimited arrows for 30 seconds at 20 yards.
* Points are counted from 1 (outer white) to 10 (inner gold).
* At the beginning of the timed round archers may have their first arrow nocked, drawn and aimed.  If you let go an arrow early or late in the timed round, you lose your highest scoring arrow for that round.
* Children's distances: Children 10 years of age and under which shoot 20, 15, 10yds
* Youth distances: Children 13 years of age and under which shoot 30, 20, 10yds

In all of the Youth Divisions, the age of the child is counted as of the start of any competition.

# Drachenwald round
The Drachenwald round was devised to provide a more authentic standard competition for the Kingdom. This competition rewards both shooting skill and the use of authentic equipment. 

It consists of three parts:
1. Clout
   This is a long range, timed round which simulates military field archery. Arrows are shot into the target that represents a besieged castle or an element of enemy soldiers arrayed for battle. The target is a circle on the ground, 10 yards in diameter. It can be made of hay bales or other suitable materials.  
   The shooting line is marked approximately 100 yards from the line to the nearest point on the target circle. If this distance is not feasible at the site, reduce it to a minimum of 80 yards, with a corresponding decrease in size of the target circle to 8 yards.  
   Archers shoot an unlimited number of arrows for one full minute. Each arrow that falls inside the target circle scores 2 points. If an archer looses an arrow in the timed round before told to do so or after the 60 seconds are up, they lose the value of one of their scoring arrows.

2. Shooting at the mark
   This is a short-range round based on precision, having more in common with hunting than with military archery.  
   The target is a piece of paper 21 x 21 cm (8¼ x 8¼inches). To make this target, fold a sheet of standard European A4 stationery in half (to A5 size), then cut off one edge to make it square. The paper may be oriented on the target any way you like.  
   Archers shoot one round of 6 arrows each at 40, 30, and 20 yards without a time limit. Each arrow that hits the mark scores 2 points. This yields a maximum of 36 points.  
   
3. Authenticity
   Using, and making, authentic equipment may also yield an archer points. A maximum of 9 points is available without even hitting a target, as follows:
   * Bow: 3 points to all archers who shoot an English longbow (D shaped without a sight window), a self bow, or any other bow which can be documented as more medieval than a modern recurve or fibreglass laminated longbow.  
   * Arrows: 3 points are awarded for using arrows with self nocks.   
   * Construction: 3 points are awarded to all archers who either  
    a. have built their own bow themselves or  
    b. have built their own period- style arrows (with self nocks and tied-on feathers).

# Portsmouth round 

The most common indoor competition in Drachenwald.

* The Portsmouth uses a standard 5-ringed 60 cm FITA target.
* Archers shoot 10 ends of 6 arrows at 20 yards.
* Points are counted from 1 (outer white) to 10 (inner gold).
* All age groups shoot from the same distance.
 
# Winter/Summer Competition
 
The Annual competition is split into two season’s competitions.

[2018 to 2019 Winter competition results to date]({{ site.baseurl }}{% link offices/archery-thrown-marshal/files/2018-2019-winter-competition-results.xls %})  

## Winter season

* Runs from October Crown Tournament to April Crown Tournament.
* Format is a Portsmouth Round.
* Submit all scores to your regional Target Archery Marshal.
* All Shoots must take place at events or official practices.
 
## Summer season

* Runs from April Crown Tournament to October Crown Tournament
* Format is a Royal Round.
* Submit all scores to your regional Target Archery Marshal
* All Shoots must take place at events or official practices.

## Previous winners of the Drachenwald Competition
 
2018/18 - Vallittu af Hukka (Summer)  
2017/18 - Rakonzay Gergely (Winter)  
2017/17 - Vallittu af Hukka  (Summer)  
2016/17 - Rakonczay Gergely  (Winter)  
2016/16 - Yannick of Normandy (Summer)  
2015/16 - Rakonczay Gergely (Winter)  
2015/15 - Yannick of Normandy (Summer)  
2014/15 - Yannick of Normandy (Winter)  
2013/14 - Pól Ó Briain (Annual competition)  
2012/13 - Pól Ó Briain (Annual competition)  
2011/12 - Pól Ó Briain (Annual competition)  
2010/11 - Pól Ó Briain (Annual competition)  